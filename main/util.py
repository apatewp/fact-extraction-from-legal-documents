"""General utility functions"""
import random

import spacy
import torch
from nltk.corpus import wordnet as wn

nlp = spacy.load("fr_core_news_sm")

def getBestSplit(binary_sequence, gamma=1):
    """
    returns best split between facts and analysis sections
    in : list of integers for candidate indices such as [1,1,0,1,0] for instance
    out : integer, position of the last paragraph in be included in the fact section
    """
    purities = {}
    for i in range(1, len(binary_sequence)+1):
        substring = binary_sequence[:i]
        purity = sum(substring)/len(substring)
        purities[len(substring)] = purity
#     print(purities)
    purities = {key:value for key, value in purities.items() if value != 1}
    try:
        index = max(purities, key=lambda key: purities[key])
    except ValueError:
        print("ValueError! Skipping...")
        return len(binary_sequence)//2
    index = int(index*gamma)
    return int(torch.sum(binary_sequence[:index]).item())

def getBestSplitLastOne(binary_sequence, gamma=1):
    """
    returns best split between facts and analysis sections by returning
    the index of the last 1 in the sequence
    in : binary sequence of integers
    out : integer, position of the last 1 in the input binary sequence
    """
    return_index = 0
    n = len(binary_sequence)
    for i in range(n-1,0,-1):
        if binary_sequence[i] == 1:
            return_index = i
            break

    return return_index

def padSentence(sentence, max_len=50):
    '''
    Pads tensor representation of sentence with zeros
    '''
    padding = max_len - len(sentence)
    sentence = torch.nn.functional.pad(sentence, pad=(0, padding), mode='constant', value=0)
    return sentence

def getOffsets(offsets):
    of = {
    "<-4" : 0,
    "-4" : 0,
    "-3" : 0,
    "-2" : 0,
    "-1" : 0,
    "0" : 0,
    "1" : 0,
    "2" : 0,
    "3" : 0,
    "4" : 0,
    ">4" : 0}
    for n in offsets:
        if n == 0: of["0"] += 1
        elif n == 1: of["1"] += 1
        elif n == 2: of["2"] += 1
        elif n == 3: of["3"] += 1
        elif n == 4: of["4"] += 1
        elif n > 4: of[">4"] += 1
        elif n == -1: of["-1"] += 1
        elif n == -2: of["-2"] += 1
        elif n == -3: of["-3"] += 1
        elif n ==-4: of["-4"] += 1
        elif n < -4: of["<-4"] += 1
    return of

class Reduced(torch.nn.Module):
    """
    Custom module that reduces the dimensionality
    of a camembert sentence embedding down to 64
    """
    def __init__(self,camembert):
        super(Reduced,self).__init__()
        self.camembert = camembert
        self.reduce = torch.nn.Linear(in_features=768, out_features=64)
        
    def forward(self,x):
        x = self.camembert(x)[1]
        x = self.reduce(x)
        return x

class camemWRAP(torch.nn.Module):
    """
    Custom module that wraps a camembert object
    and returns the sentence embedding
    """
    def __init__(self,camembert):
        super(camemWRAP, self).__init__()
        self.camembert = camembert
    
    def forward(self, x):
        return self.camembert(x)[1]

def substituteLexeme(word, pos_tag):
    """
    Helper function that receives a word and returns 
    a randomly selected synonym
    """
    if pos_tag == "NOUN":
        pos_ = wn.NOUN
    elif pos_tag == "ADJ":
        pos_ = wn.ADJ
    else: 
        return word
    synsets = [synset.lemma_names('fra') for synset in wn.synsets(word, lang='fra', pos=pos_)]
    try:
        synsets = max(synsets)
    except ValueError:
        synsets = []
    if len(synsets) != 0:
        subs_word = random.choice(synsets)
    else:
        return word
    return subs_word

def generateTrainingInstance(training_instance):
    """
    generates artificial training instance based on PLSDA (Xiang et al. 2020)
    IN: training_instance - a dict object with two keys, 'facts' and 'non_facts'
    each key is associated to a list of sentences
    """
    new_instance = {"facts" : [], "non_facts" : []}
    facts = training_instance["facts"]
    non_facts = training_instance["non_facts"]
    for sentence in facts:
        sentence = nlp(sentence)
        sentence = [(w.text, w.pos_) for w in sentence]
        new_sentence = []
        for word, pos in sentence:
            word = substituteLexeme(word, pos)
            new_sentence.append(word)
        new_sentence = " ".join(new_sentence)
        new_instance["facts"].append(new_sentence)
    for sentence in non_facts:
        sentence = nlp(sentence)
        sentence = [(w.text, w.pos_) for w in sentence]
        new_sentence = []
        for word, pos in sentence:
            word = substituteLexeme(word, pos)
            new_sentence.append(word)
        new_sentence = " ".join(new_sentence)
        new_instance["non_facts"].append(new_sentence)    
    return new_instance

def generateTrainingInstanceSentence(sentence):
    """
    generates artificial training instance based on PLSDA (Xiang et al. 2020)
    IN: sentence : str
    OUT: sentence : str
    """
    sentence = nlp(sentence)
    sentence = [(w.text, w.pos) for w in sentence]
    new_sentence = []
    for word, pos in sentence:
        word = substituteLexeme(word,pos)
        new_sentence.append(word)
    new_sentence = " ".join(new_sentence)
    return new_sentence
