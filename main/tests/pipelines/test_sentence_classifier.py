from typing import List

import gensim
import pandas
import spacy

from main.pipelines.sentence_classifier_pipelines import SentenceClassifierPipeline, GetSentencesPipeline, \
    AugmentSentencesPipeline, \
    SplitSentencesPipeline, GetOffsetsPipeline, CreateGoldStandardPipeline
from main.tests.test_data import TEST_SENTENCES_FOR_DATAFRAME, TEST_DOCUMENTS, TEST_SPLITTING_INDICES
from main.modules.sentence_classifier_module import SentenceClassifier as SentClass

DATA_INPUTS = pandas.DataFrame(TEST_SENTENCES_FOR_DATAFRAME, columns=['Facts', 'Analyses'])
TOKENISER = spacy.load("fr_core_news_sm")
EMB_MODEL = gensim.models.KeyedVectors.load_word2vec_format(
    "~/PycharmProjects/fact-extraction-from-legal-documents/frWac_non_lem_no_postag_no_phrase_200_cbow_cut100.bin",
    binary=True,
    unicode_errors='ignore'
)


def test_sentenceclassifier():
    get_sents_pipeline = GetSentencesPipeline(tokeniser=TOKENISER)
    (sents, annotations), splitting_indices = get_sents_pipeline.transform(DATA_INPUTS)
    assert all(isinstance(sent, str) for sent in sents)
    assert all(isinstance(annotation, int) for annotation in annotations)
    assert all(isinstance(index, int) for index in splitting_indices)

    augment_sents_pipeline = AugmentSentencesPipeline(tokeniser=TOKENISER)
    _, (sents, annotations) = augment_sents_pipeline.fit_transform(data_inputs=sents, expected_outputs=annotations)
    assert all(isinstance(sent, str) for sent in sents)
    assert all(isinstance(annotation, int) for annotation in annotations)

    sent_classification_pipeline = SentenceClassifierPipeline(tokeniser=TOKENISER,
                                                              emb_model=EMB_MODEL,
                                                              classify_sentences__n_epochs=1,
                                                              augment=0)
    _, prediction = sent_classification_pipeline.fit_transform(data_inputs=sents, expected_outputs=annotations)
    model = sent_classification_pipeline.steps['classify_sentences'].model
    assert isinstance(model, SentClass)
    assert all(isinstance(x, float) for x in prediction)

    split_sentences_pipeline = SplitSentencesPipeline(model=model, tokeniser=TOKENISER, emb_model=EMB_MODEL, gamma=[.8, 1])
    indices_list = split_sentences_pipeline.transform(data_inputs=TEST_DOCUMENTS)
    assert all([isinstance(n, int) for n in indices] for indices in indices_list)

    get_offsets_pipeline = GetOffsetsPipeline()
    offsets_list = get_offsets_pipeline.transform((indices_list, TEST_SPLITTING_INDICES))
    assert isinstance(offsets_list, List)
    assert all(isinstance(offsets, dict) for offsets in offsets_list)

    create_goldstandard_pipeline = CreateGoldStandardPipeline()
    documents = create_goldstandard_pipeline.transform(data_inputs=DATA_INPUTS)
    assert all(isinstance(doc, str) for doc in documents)

