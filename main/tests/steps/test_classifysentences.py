from typing import List

import gensim
import pytest
import torch
import spacy
from main.pipelines.steps.sentence_classifier_steps.classify_sentences import SentenceDataset, ClassifySentences
from main.tests.test_data import TEST_TAGGED_VECTORISED_SENTENCES

EMB_MODEL = gensim.models.KeyedVectors.load_word2vec_format(
    "~/PycharmProjects/fact-extraction-from-legal-documents/frWac_non_lem_no_postag_no_phrase_200_cbow_cut100.bin",
    binary=True,
    unicode_errors='ignore')
TOKENISER = spacy.load("fr_core_news_sm")
MOCK_AUGMENT = 0
VECTORISED_SENTS = [sent[0] for sent in TEST_TAGGED_VECTORISED_SENTENCES]
ANNOTATIONS = [sent[1] for sent in TEST_TAGGED_VECTORISED_SENTENCES]


def test_sentencedataset():
    max_len = 45
    ds = SentenceDataset(docs=VECTORISED_SENTS, annotations=ANNOTATIONS, max_len=max_len)
    assert len(ds) == len(TEST_TAGGED_VECTORISED_SENTENCES)
    assert all(isinstance(d[0], torch.Tensor) for d in ds)
    assert all(isinstance(d[1], torch.Tensor) for d in ds)
    ds = SentenceDataset(docs=VECTORISED_SENTS, annotations=ANNOTATIONS, n_sentences=2)
    assert all(len(d[0] == max_len) for d in ds)
    assert len(ds) == 2
    with pytest.raises(IndexError):
        assert ds[len(TEST_TAGGED_VECTORISED_SENTENCES)-1]


def test_fit():
    step = ClassifySentences(emb_model=EMB_MODEL, tokeniser=TOKENISER, augment=MOCK_AUGMENT)
    assert isinstance(step, ClassifySentences)
    step = step.fit(VECTORISED_SENTS, ANNOTATIONS)
    assert isinstance(step, ClassifySentences)


def test_transform():
    step = ClassifySentences(emb_model=EMB_MODEL, tokeniser=TOKENISER, augment=MOCK_AUGMENT)
    with pytest.raises(AssertionError):
        assert step.transform(VECTORISED_SENTS)
    step.fit(VECTORISED_SENTS, ANNOTATIONS)
    output = step.transform(VECTORISED_SENTS)
    assert isinstance(output, List)
    assert all(isinstance(p, float) for p in output)


def test_fittransform():
    step = ClassifySentences(emb_model=EMB_MODEL, tokeniser=TOKENISER, augment=MOCK_AUGMENT)
    fitted_step, predictions = step.fit_transform(VECTORISED_SENTS, ANNOTATIONS)
    assert all(isinstance(p, float) for p in predictions)
    assert isinstance(fitted_step, ClassifySentences)

