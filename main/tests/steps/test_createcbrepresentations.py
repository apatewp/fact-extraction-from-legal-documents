import pickle
from os.path import expanduser

import spacy

from main.pipelines.steps.recurrent_architectures_steps.create_camembert_representations import \
    CreateCamembertRepresentations
from main.tests.test_data import TEST_DOCUMENTS, TEST_SPLITTING_INDICES

TOKENISER = spacy.load("fr_core_news_sm")

def test_createcamembertrepresentations():
    # TODO make sure this thing works
    step = CreateCamembertRepresentations(tokeniser=TOKENISER)
    step.fit_transform(data_inputs=TEST_DOCUMENTS, expected_outputs=TEST_SPLITTING_INDICES)
    for i in range(len(TEST_DOCUMENTS)):
        d = []
        loc = "{}/PycharmProjects/fact-extraction-from-legal-documents/data/camembert_representations/{}.pickle".format(expanduser("~"), i)
        with open(loc, "rb") as file:
            while True:
                try:
                    d += [pickle.load(file)]
                except EOFError:
                    break
        sent = d[:-1]
        annotation = d[-1]
        assert len(sent) == len(list(TOKENISER(TEST_DOCUMENTS[i]).sents))
        assert len(annotation) == 1