import spacy
import torch

from main.pipelines.steps.recurrent_architectures_steps.binarise_documents import DocumentDataset
from main.tests.test_data import TEST_DOCUMENTS

DS_LOC = "/Users/feasinde/PycharmProjects/fact-extraction-from-legal-documents/data/camembert_representations"
tokeniser = spacy.load("fr_core_news_sm")
lengths = [len(list(doc.sents)) for doc in [tokeniser(document) for document in TEST_DOCUMENTS]]

def test_documentdataset():
    dataset = DocumentDataset(ds_loc=DS_LOC)
    sentences = [dataset[i][0] for i in range(len(dataset))]
    annotations = [dataset[i][1] for i in range(len(dataset))]
    assert all(sentences[i].shape == torch.Size([lengths[i], 768]) for i in range(len(dataset)))
    assert all(annotation.shape == torch.Size([1]) for annotation in annotations)
