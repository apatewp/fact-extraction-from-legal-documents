from typing import List

import gensim
import spacy
import torch

from main.modules.sentence_classifier_module import SentenceClassifier
from main.pipelines.steps.split_sentences import SplitSentences
from main.tests.test_data import TEST_DOCUMENTS

EMB_MODEL = gensim.models.KeyedVectors.load_word2vec_format(
    "~/PycharmProjects/fact-extraction-from-legal-documents/frWac_non_lem_no_postag_no_phrase_200_cbow_cut100.bin",
    binary=True,
    unicode_errors='ignore')
EMB_TENSOR = torch.FloatTensor(EMB_MODEL.vectors)
TEST_MODEL = SentenceClassifier(embeddings_tensor=EMB_TENSOR)
TOKENISER = spacy.load("fr_core_news_sm")


def test_splitsentences():
    step = SplitSentences(model=TEST_MODEL, tokeniser=TOKENISER, emb_model=EMB_MODEL, gamma=[.8, 1])
    outputs = step.transform(data_inputs=TEST_DOCUMENTS)
    assert all(isinstance(output, List) for output in outputs)
    assert all([isinstance(n, int) for n in output] for output in outputs)
    print(outputs)