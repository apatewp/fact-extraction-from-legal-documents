import torch

TEST_TAGGED_SENTENCES = [
    ("Pour moi tu es comme des os", 1),
    ("Por moi tu es comme des membres.", 1),
    ("Deux hommes entre tous les hommes ont le droit de repondre.", 0),
    ("Les Copains d'abord est ma boulangerie favorite", 1),
    ("Ceci n'est pas une pipe", 0),
    ("J’espère également que son puissant appareil a vaincu la mer dans son gouffre le plus terrible, et que le Nautilus a survécu là où tant de navires ont péri", 0),
    ("Cet enlèvement, si brutalement exécuté, s’était accompli avec la rapidité de l’éclair.", 1),
    ("Mes compagnons et moi, nous n’avions pas eu le temps de nous reconnaître.", 0),
    ("Je ne sais ce qu’ils éprouvèrent en se sentant introduits dans cette prison flottante", 1),
    ("À peine l’étroit panneau fut-il refermé sur moi, qu’une obscurité profonde m’enveloppa", 0),
    ("Mes yeux, imprégnés de la lumière extérieure, ne purent rien percevoir.", 1),
    ("Je sentis mes pieds nus se cramponner aux échelons d’une échelle de fer", 0),
    ("Ned Land et Conseil, vigoureusement saisis, me suivaient.", 1),
    ("Nous étions seuls.", 0),
    ("Je ne pouvais le dire, à peine l’imaginer. ", 1),
    ("Cependant, Ned Land, furieux de ces façons de procéder, donnait un libre cours à son indignation. ", 0),
    ("Calmez-vous, ami Ned, calmez-vous, répondit tranquillement Conseil.", 1),


]

TEST_TAGGED_VECTORISED_SENTENCES = [
    (torch.tensor([15, 141, 113, 798, 44, 8, 5048]), 1),
    (torch.tensor([5440, 141, 113, 798, 44, 8, 315, 0]), 1),
    (torch.tensor([62, 481, 69, 59, 6, 481, 49, 4, 133, 1, 13376, 0]), 1),
    (torch.tensor([6, 7233, 9, 10967, 13, 151, 12430, 17464]), 1),
    (torch.tensor([979, 36, 13, 24, 14, 19523]), 0),
    (torch.tensor([15, 141, 113, 798, 44, 8, 5048]), 1),
    (torch.tensor([5440, 141, 113, 798, 44, 8, 315, 0]), 1),
    (torch.tensor([62, 481, 69, 59, 6, 481, 49, 4, 133, 1, 13376, 0]), 1),
    (torch.tensor([6, 7233, 9, 10967, 13, 151, 12430, 17464]), 1),
    (torch.tensor([979, 36, 13, 24, 14, 19523]), 0),
    (torch.tensor([15, 141, 113, 798, 44, 8, 5048]), 1),
    (torch.tensor([5440, 141, 113, 798, 44, 8, 315, 0]), 1),
    (torch.tensor([62, 481, 69, 59, 6, 481, 49, 4, 133, 1, 13376, 0]), 1),
    (torch.tensor([6, 7233, 9, 10967, 13, 151, 12430, 17464]), 1),
    (torch.tensor([979, 36, 13, 24, 14, 19523]), 0),
    (torch.tensor([15, 141, 113, 798, 44, 8, 5048]), 1),
    (torch.tensor([5440, 141, 113, 798, 44, 8, 315, 0]), 1),
    (torch.tensor([62, 481, 69, 59, 6, 481, 49, 4, 133, 1, 13376, 0]), 1),
    (torch.tensor([6, 7233, 9, 10967, 13, 151, 12430, 17464]), 1),
    (torch.tensor([979, 36, 13, 24, 14, 19523]), 0)
]

TEST_SENTENCES_FOR_DATAFRAME = [
    [
        "Ceci n'est pas une lune. Ceci n'est pase un pipe.",
        "Pour moi tu es comme des os\nPour moi tu es comme des membres.",
    ],
    [
        "Deux hommes entre tous les hommes ont le droit de repondre: le capitain Nemo et moi.",
        "C'est vache comme il pleut. Rentrez chez-vous, monsieur.",
    ],
    [
        "C’était une bibliothèque. De hauts meubles en palissandre noir, incrustés de cuivres, supportaient sur leurs larges rayons un grand nombre de livres uniformément reliés. Ils suivaient le contour de la salle et se terminaient à leur partie inférieure par de vastes divans, capitonnés de cuir marron, qui offraient les courbes les plus confortables. De légers pupitres mobiles, en s’écartant ou se rapprochant à volonté, permettaient d’y poser le livre en lecture. Au centre se dressait une vaste table, couverte de brochures, entre lesquelles apparaissaient quelques journaux déjà vieux. La lumière électrique inondait tout cet harmonieux ensemble, et tombait de quatre globes dépolis à demi engagés dans les volutes du plafond. Je regardais avec une admiration réelle cette salle si ingénieusement aménagée, et je ne pouvais en croire mes yeux.",
        "Enfin, après deux heures de marche, nous avions atteint une profondeur de trois cents mètres environ, c’est-à-dire la limite extrême sur laquelle le corail commence à se former. Mais là, ce n’était plus le buisson isolé, ni le modeste taillis de basse futaie. C’était la forêt immense, les grandes végétations minérales, les énormes arbres pétrifiés, réunis par des guirlandes d’élégantes plumarias, ces lianes de la mer, toutes parées de nuances et de reflets. Nous passions librement sous leur haute ramure perdue dans l’ombre des flots, tandis qu’à nos pieds, les tubipores, les méandrines, les astrées, les fongies, les cariophylles, formaient un tapis de fleurs, semé de gemmes éblouissantes."

    ]
]

TEST_DOCUMENTS = [
    "Deux hommes entre tous les hommes ont le droit de repondre. Le capitain Nemo et moi.",
    "Pour moi tu es comme des os. Pour moi tu es comme des membres. L'energie du vent!",
    "C’était une bibliothèque. De hauts meubles en palissandre noir, incrustés de cuivres, supportaient sur leurs larges rayons un grand nombre de livres uniformément reliés. Ils suivaient le contour de la salle et se terminaient à leur partie inférieure par de vastes divans, capitonnés de cuir marron, qui offraient les courbes les plus confortables. De légers pupitres mobiles, en s’écartant ou se rapprochant à volonté, permettaient d’y poser le livre en lecture. Au centre se dressait une vaste table, couverte de brochures, entre lesquelles apparaissaient quelques journaux déjà vieux. La lumière électrique inondait tout cet harmonieux ensemble, et tombait de quatre globes dépolis à demi engagés dans les volutes du plafond. Je regardais avec une admiration réelle cette salle si ingénieusement aménagée, et je ne pouvais en croire mes yeux.",
    "Le capitaine Nemo me salua. Je restai seul, absorbé dans mes pensées. Toutes se portaient sur ce commandant du Nautilus. Saurais-je jamais à quelle nation appartenait cet homme étrange qui se vantait de n’appartenir à aucune ? Cette haine qu’il avait vouée à l’humanité, cette haine qui cherchait peut-être des vengeances terribles, qui l’avait provoquée ? Etait-il un de ces savants méconnus, un de ces génies « auxquels on a fait du chagrin, » suivant l’expression de Conseil, un Galilée moderne, ou bien un de ces hommes de science comme l’Américain Maury, dont la carrière a été brisée par des révolutions politiques ? Je ne pouvais encore le dire. Moi que le hasard venait de jeter à son bord, moi dont il tenait la vie entre les mains, il m’accueillait froidement, mais hospitalièrement. Seulement, il n’avait jamais pris la main que je lui tendais. Il ne m’avait jamais tendu la sienne.",
    "Enfin, après deux heures de marche, nous avions atteint une profondeur de trois cents mètres environ, c’est-à-dire la limite extrême sur laquelle le corail commence à se former. Mais là, ce n’était plus le buisson isolé, ni le modeste taillis de basse futaie. C’était la forêt immense, les grandes végétations minérales, les énormes arbres pétrifiés, réunis par des guirlandes d’élégantes plumarias, ces lianes de la mer, toutes parées de nuances et de reflets. Nous passions librement sous leur haute ramure perdue dans l’ombre des flots, tandis qu’à nos pieds, les tubipores, les méandrines, les astrées, les fongies, les cariophylles, formaient un tapis de fleurs, semé de gemmes éblouissantes."
]

TEST_SPLITTING_INDICES = [int(len(d)/2) for d in TEST_DOCUMENTS]
