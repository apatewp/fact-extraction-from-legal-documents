import torch

from main.modules.recurrent_architecture_modules import AttentionEncoder, AttentionDecoder, AttentionEncoderDecoder, \
    GRULinear, LSTMLinear

VECTOR_SIZE = 768

TEST_INPUT = torch.rand(1, 10, VECTOR_SIZE)
TEST_HIDDEN = torch.rand(1, VECTOR_SIZE)
EXPECTED_SIZE = torch.Size([TEST_INPUT.shape[1]])

def test_attn_encoder():
    encoder = AttentionEncoder(hidden_size=VECTOR_SIZE)
    output = encoder(TEST_INPUT)
    test_size = TEST_INPUT.shape
    assert output.shape == torch.Size([test_size[1], test_size[2]])
    assert isinstance(output, torch.Tensor)

def test_attn_decoder():
    decoder = AttentionDecoder(hidden_size=VECTOR_SIZE)
    output, hidden = decoder(TEST_INPUT.squeeze(), TEST_HIDDEN)
    assert output.shape == torch.Size([1])
    assert hidden.shape == TEST_HIDDEN.shape

def test_attn_encoder_decoder():
    model = AttentionEncoderDecoder(hidden_size=VECTOR_SIZE, max_len=128, device=torch.device('cpu'))
    output = model(TEST_INPUT)
    assert output.shape == EXPECTED_SIZE

def test_GRULinear():
    model = GRULinear(embedding_size=VECTOR_SIZE)
    output = model(TEST_INPUT)
    assert output.shape == EXPECTED_SIZE

def test_LSTMLinear():
    model = LSTMLinear(embedding_size=VECTOR_SIZE)
    output = model(TEST_INPUT)
    assert  output.shape == EXPECTED_SIZE