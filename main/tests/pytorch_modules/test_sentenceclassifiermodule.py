import gensim
import torch
from torch.utils.data import DataLoader

from main.modules.sentence_classifier_module import SentenceClassifier
from main.pipelines.steps.sentence_classifier_steps.classify_sentences import PadSequence
from main.tests.test_data import TEST_TAGGED_VECTORISED_SENTENCES

emb_model = gensim.models.KeyedVectors.load_word2vec_format(
    "~/PycharmProjects/fact-extraction-from-legal-documents/frWac_non_lem_no_postag_no_phrase_200_cbow_cut100.bin",
    binary=True,
    unicode_errors='ignore'
)

EMB_TENSOR = torch.tensor(emb_model.vectors)

def test_sentenceclassifiermodule():
    model = SentenceClassifier(embeddings_tensor=EMB_TENSOR)
    dataloader = DataLoader(TEST_TAGGED_VECTORISED_SENTENCES, batch_size=3, collate_fn=PadSequence())
    for datum in dataloader:
        sentence, annotation, lengths = datum
        output = model(sentence, lengths)
