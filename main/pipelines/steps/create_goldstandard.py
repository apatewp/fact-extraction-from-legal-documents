from typing import List

import pandas
from neuraxle.base import BaseTransformer
from tqdm import tqdm


class CreateGoldStandard(BaseTransformer):

    def __init__(self):
        super().__init__()

    def transform(self, df: pandas.DataFrame) -> List[str]:
        gold_standards = []
        for i in tqdm(range(len(df)), desc="Creating gold standard"):
            facts, non_facts = df.loc[i, "Facts"], df.loc[i, "Analyses"]
            document = "{} {}".format(facts, non_facts)
            gold_standards += [document]
        return gold_standards

    def inverse_transform(self, processed_outputs):
        pass