from typing import List, Any

import gensim
import torch
from neuraxle.base import BaseTransformer
from torch.utils.data import DataLoader
from tqdm import tqdm

from main.modules.pytorch_utils import get_device
from main.pipelines.steps.sentence_classifier_steps.classify_sentences import SentenceClassifier, \
    pad_sequence_unlabelled
from main.pipelines.steps.sentence_classifier_steps.vectorise_sentences import tensorFromSentence


class SplitSentences(BaseTransformer):
    def __init__(self,
                 model: SentenceClassifier,
                 tokeniser: Any,
                 emb_model: gensim.models.keyedvectors.Word2VecKeyedVectors,
                 gamma: List[float]):
        super().__init__()
        self.device = get_device()
        self.model = model
        self.model.eval()
        self.model.to(self.device)
        self.tokeniser = tokeniser
        self.word2index = {token: token_index for token_index, token in enumerate(emb_model.index2word)}
        self.gamma = gamma

    def transform(self, data_inputs: List[str]) -> List[List[int]]:
        output = [self.tokeniser(doc) for doc in tqdm(data_inputs, desc="Tokenising sentences for splitting")]
        output = [[tensorFromSentence(
            sent.text, tokeniser=self.tokeniser, word2index=self.word2index
        ) for sent in doc.sents] for doc in tqdm(output, desc="Vectorising sentences for splitting")]
        output = [DataLoader(doc, batch_size=512, collate_fn=pad_sequence_unlabelled) for doc in output]
        with torch.no_grad():
            output = [[self.model(sents.to(self.device), lengths) for sents, lengths in doc] for doc in tqdm(output, desc="Classifying sentences")]
            output = [doc[0] for doc in output]
            output = [[torch.squeeze(sent).item() for sent in doc] for doc in output]
        output = [[round(x) for x in doc] for doc in output]
        output = [[self._getBestSplit(doc, gamma=gamma) for doc in tqdm(output, desc="Retrieving splitting indices")] for gamma in tqdm(self.gamma)]
        return output

    def inverse_transform(self, processed_outputs):
        pass

    @staticmethod
    def _getBestSplit(input_list: List[int], gamma) -> int:
        """
        returns best split between facts and analysis sections
        in : list of integers for candidate indices such as [1,1,0,1,0] for instance
        out : integer, position of the last paragraph in be included in the fact section
        """
        purities = {}
        for i in range(1, len(input_list) + 1):
            substring = input_list[:i]
            purity = sum(substring) / len(substring)
            purities[len(substring)] = purity
        purities = {key: value for key, value in purities.items() if value != 1}
        try:
            index = max(purities, key=lambda key: purities[key])
        except ValueError:
            print("ValueError! Skipping...")
            return len(input_list) // 2
        index = int(index * gamma)
        return int(sum(input_list[:index]))
        pass