import numpy
import torch
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
from torch.nn import MSELoss
from torch.optim import AdamW
from torch.optim.lr_scheduler import MultiStepLR
from torch.utils.data import random_split, DataLoader
from tqdm import tqdm

from main.modules.pytorch_utils import get_device

device = get_device()

class NeuralTrainingMixin:

    @staticmethod
    def _test(target_tensor, prediction_tensor):
        t = target_tensor.cpu().detach().numpy()
        p = prediction_tensor.cpu().detach().numpy()
        p = numpy.round(p)
        try:
            acc = accuracy_score(t, p)
            pre = precision_score(t, p)
            rec = recall_score(t, p)
            f1 = f1_score(t, p)
        except ValueError:
            print("ValueError!")
            print("t, p: {}, {}".format(t, p))
            print("Skipping this training example...")
            acc = pre = rec = f1 = 0
        return acc, pre, rec, f1

    @staticmethod
    def _train(input_tensor, target, model, optimiser, criterion, clip, lengths, mode="train"):
        if mode == "train":
            model.train()
            optimiser.zero_grad()
            model.to(device)
            prediction = model(input_tensor.to(device), lengths)
            loss = criterion(prediction, target.to(device))
            loss.backward()
            torch.nn.utils.clip_grad_norm_(model.parameters(), clip)
            optimiser.step()
            return loss.item(), prediction
        elif mode == "validate":
            model.eval()
            model.to(device)
            with torch.no_grad():
                prediction = model(input_tensor.to(device), lengths)
                loss = criterion(prediction, target.to(device))
                return loss.item(), prediction
        else:
            raise ValueError("Unexpected train mode: {}".format(mode))

    def _trainIterations(self, dataset, collate_fn):
        tr = len(dataset)
        vd = int(tr * .1)
        tr = tr - vd
        training_set, validation_set = random_split(dataset, [tr, vd])
        optimiser = AdamW(self.model.parameters(), lr=self.hyperparams['learning_rate'],
                          weight_decay=self.hyperparams['weight_decay'])
        scheduler = MultiStepLR(optimizer=optimiser, milestones=self.hyperparams['lr_step'],
                                gamma=self.hyperparams['lr_gamma'])
        criterion = MSELoss()
        train_dl = DataLoader(training_set, batch_size=self.hyperparams['batch_size'], collate_fn=collate_fn)
        valid_dl = DataLoader(validation_set, batch_size=self.hyperparams['batch_size'], collate_fn=collate_fn)
        with open("output_augm_{}".format(self.augment), "w") as f:
            f.write(
                "tr_acc\t\ttr_prec\t\ttr_rec\t\ttr_f1\t\tvd_acc\t\tvd_prec\t\tvd_rec\t\tvd_f1\n"
            )
        for epoch in tqdm(range(1, self.hyperparams['n_epochs'] + 1), desc='Epochs', leave=True):
            train_acc = []
            train_prec = []
            train_rec = []
            train_f1 = []
            train_dl = tqdm(train_dl, desc='Training', leave=False)
            for datum in train_dl:
                input_tensor = datum[0]
                target = datum[1].squeeze()
                lengths = datum[2]
                _, prediction = self._train(input_tensor, target, self.model, optimiser, criterion, self.hyperparams['clip'], lengths)
                assert target.shape == prediction.shape, "Target shape and prediction shape do not match"
                acc, prec, rec, f1 = self._test(target, prediction)
                train_acc += [acc]
                train_prec += [prec]
                train_rec += [rec]
                train_f1 += [f1]
            train_acc = sum(train_acc)/len(train_acc)
            train_prec = sum(train_prec) / len(train_prec)
            train_rec = sum(train_rec) / len(train_rec)
            train_f1 = sum(train_f1) / len(train_f1)

            valid_acc = []
            valid_prec = []
            valid_rec = []
            valid_f1 = []
            valid_dl = tqdm(valid_dl, desc='Validating', leave=False)
            for datum in valid_dl:
                input_tensor = datum[0]
                target = datum[1].squeeze()
                lengths = datum[2]
                _, prediction = self._train(input_tensor, target, self.model, optimiser, criterion,
                                            self.hyperparams['clip'], lengths, mode="validate")
                assert target.shape == prediction.shape, "Target shape and prediction shape do not match"
                vd_acc, vd_prec, vd_rec, vd_f1 = self._test(target, prediction)
                valid_acc += [vd_acc]
                valid_prec += [vd_prec]
                valid_rec += [vd_rec]
                valid_f1 += [vd_f1]
            valid_acc = sum(valid_acc)/len(valid_acc)
            valid_prec = sum(valid_prec) / len(valid_prec)
            valid_rec = sum(valid_rec) / len(valid_rec)
            valid_f1 = sum(valid_f1) / len(valid_f1)
            with open("output_augm_{}".format(self.augment), "a") as f:
                f.write("{:.4f}\t\t{:.4f}\t\t{:.4f}\t\t{:.4f}\t\t".format(train_acc, train_prec, train_rec, train_f1))
                f.write("{:.4f}\t\t{:.4f}\t\t{:.4f}\t\t{:.4f}\n".format(valid_acc, valid_prec, valid_rec, valid_f1))
            scheduler.step()
