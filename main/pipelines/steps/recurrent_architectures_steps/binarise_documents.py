import os
import pickle

import torch
from torch.utils.data import Dataset

from main.modules.pytorch_utils import get_device

device = get_device()

class DocumentDataset(Dataset):
    def __init__(self, ds_loc: str, ds_len: int = None):
        super(DocumentDataset, self).__init__()
        self.ds_loc = ds_loc
        self.ds_len = ds_len

    def __len__(self):
        if self.ds_len is not None:
            return self.ds_len
        return len(os.listdir(self.ds_loc))

    def __getitem__(self, item):
        if self.ds_len is not None and item >= self.ds_len:
            raise IndexError("Index out of range")
        filename = "{}/{}.pickle".format(self.ds_loc, str(item))
        doc = []
        with open(filename, "rb") as file:
            while True:
                try:
                    doc.append(pickle.load(file))
                except EOFError:
                    break
        sentences = doc[:-1]
        sentences = torch.stack(sentences)
        annotations = doc[-1]
        return sentences, annotations