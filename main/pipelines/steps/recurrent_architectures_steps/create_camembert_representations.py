import pickle
from os import getcwd
from os.path import expanduser
from typing import Any, List

import torch
import transformers
from neuraxle.base import BaseStep, _FittableStep
from tqdm import tqdm

from main.modules.pytorch_utils import get_device

DEVICE = get_device()
DATA_FOLDER = "{}/PycharmProjects/fact-extraction-from-legal-documents/data".format(expanduser("~"))


class CreateCamembertRepresentations(BaseStep):

    def __init__(self, tokeniser: Any, model='camembert-base'):
        super().__init__()
        self.sentence_tokeniser = tokeniser
        self.tokeniser = transformers.AutoTokenizer.from_pretrained(model)
        self.model = transformers.AutoModel.from_pretrained(model)
        self.model.eval()
        self.model.to(DEVICE)

    def fit(self, data_inputs: None, expected_outputs: List[int]) -> '_FittableStep':
        self.splitting_indices = expected_outputs
        return self

    def transform(self, data_inputs: List[str], data_loc: str = None) -> None:
        n_doc = 0
        for i, document in enumerate(tqdm(data_inputs, desc='Writing CamemBERT representations to disc')):
            document = self.sentence_tokeniser(document)
            with open("{}/camembert_representations/{}.pickle".format(DATA_FOLDER, n_doc), "wb") as doc:
                for sentence in document.sents:
                    sentence = str(sentence)
                    sentence = self.tokeniser.encode(sentence)
                    sentence = torch.tensor([sentence])
                    sentence = self.model(sentence.to(DEVICE))
                    sentence = sentence[1]
                    sentence.squeeze_()
                    pickle.dump(sentence.cpu(), doc)
                pickle.dump(torch.tensor([self.splitting_indices[i]]), doc)
            n_doc += 1

    def inverse_transform(self, processed_outputs):
        pass
