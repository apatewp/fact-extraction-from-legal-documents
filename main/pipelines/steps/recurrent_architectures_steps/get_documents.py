from typing import List, Any

import pandas
import spacy
from neuraxle.base import BaseTransformer


class GetDocuments(BaseTransformer):
    def __init__(self, tokeniser: Any):
        super().__init__()
        self.tokeniser = tokeniser

    def transform(self, data_inputs: pandas.DataFrame) -> (List[str], List[int]):
        documents = []
        splitting_indices = []
        for i in range(len(data_inputs)):
            facts, non_facts = data_inputs.loc[i, 'Facts'], data_inputs.loc[i, 'Analyses']
            document = facts + " " + non_facts
            documents += [document]
            facts = self.tokeniser(facts)
            splitting_indices += [len(list(facts.sents))]
        return documents, splitting_indices

    def inverse_transform(self, processed_outputs):
        pass