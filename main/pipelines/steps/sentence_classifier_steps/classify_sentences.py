from typing import List
import gensim
import torch
from neuraxle.base import BaseStep
from neuraxle.hyperparams.space import HyperparameterSamples
from torch.utils.data import DataLoader

from main.modules.pytorch_utils import get_device
from main.modules.sentence_classifier_module import SentenceClassifier
from main.pipelines.steps.neural_training_mixin import NeuralTrainingMixin

device = get_device()


class SentenceDataset(torch.utils.data.Dataset):
    def __init__(self, docs: List[torch.Tensor], annotations: List[int],
                 max_len: int = 50,
                 n_sentences: int = 0):
        self.documents = docs
        self.annotations = [torch.tensor(x) for x in annotations]
        self.max_len = max_len
        self.n_sentences = n_sentences

    def __len__(self):
        if self.n_sentences == 0:
            return len(self.documents)
        return self.n_sentences

    def __getitem__(self, item):
        if self.n_sentences != 0 and item >= self.n_sentences:
            raise IndexError("Index out of bounds for user-specified number of documents")
        sentence = self.documents[item]
        tag = self.annotations[item]
        return sentence, tag


class PadSequence:
    """
    Collate function that returns padded sequence to pass to DataLoader
    """
    def __call__(self, batch):
        sorted_batch = sorted(batch, key=lambda x: x[0].shape[0], reverse=True)
        sequences = [x[0] for x in sorted_batch]
        lengths = torch.LongTensor([len(x) for x in sequences])
        sequences_padded = torch.nn.utils.rnn.pad_sequence(sequences, batch_first=True)
        labels = torch.Tensor([x[1] for x in sorted_batch])
        return sequences_padded, labels.unsqueeze(0), lengths


def pad_sequence_unlabelled(batch):
    sequences = sorted(batch, key=lambda x: x.shape[0], reverse=True)
    lengths = torch.LongTensor([len(x) for x in sequences])
    sequences_padded = torch.nn.utils.rnn.pad_sequence(sequences, batch_first=True)
    return sequences_padded, lengths


class ClassifySentences(NeuralTrainingMixin, BaseStep):

    DEFAULT_PARAMS = HyperparameterSamples({
        'batch_size': 512,
        'n_epochs': 25,
        'learning_rate': 1e-3,
        'weight_decay': 1e-4,
        'clip': 0.2,
        'lr_step': [10, 13],
        'lr_gamma': 0.1,
    })

    def __init__(self, emb_model: gensim.models.keyedvectors.Word2VecKeyedVectors, tokeniser, augment, save_model=False):
        super().__init__()
        self.emb_model = emb_model
        emb_vectors = torch.tensor(emb_model.vectors)
        self.set_hyperparams(self.DEFAULT_PARAMS)
        self.save_model = save_model
        self.model = SentenceClassifier(embeddings_tensor=emb_vectors)
        self._isFit = False
        self.tokeniser = tokeniser
        self.word2index = {token: token_index for token_index, token in enumerate(self.emb_model.index2word)}
        self.augment = augment

    def fit(self, vectorised_sentences: List[torch.Tensor], annotations: List[int]) -> 'ClassifySentences':
        self._isFit = True
        dataset = SentenceDataset(vectorised_sentences, annotations)
        self._trainIterations(dataset=dataset, collate_fn=PadSequence())
        return self

    def transform(self, sents: List[torch.Tensor]) -> List[float]:
        assert self._isFit, "Cannot transform data without fitting first"
        vectorised_sents = []
        sents_loader = DataLoader(sents, collate_fn=pad_sequence_unlabelled, batch_size=self.hyperparams['batch_size'])
        for sent in sents_loader:
            input_, lengths = sent
            self.model.to(device)
            output = self.model(input_.to(device), lengths)
            output.squeeze_()
            vectorised_sents += output.tolist()
        return vectorised_sents

    def inverse_transform(self, processed_outputs):
        pass
