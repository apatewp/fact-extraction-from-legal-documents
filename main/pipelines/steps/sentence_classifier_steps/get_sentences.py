from typing import List, Tuple

import pandas
from neuraxle.base import BaseTransformer
from tqdm import tqdm


class GetSentences(BaseTransformer):
    def __init__(self, tokeniser):
        super().__init__()
        self.sentence_tokeniser = tokeniser

    def transform(self, df: pandas.DataFrame, to_file: bool = False) -> (Tuple[List[str], List[int]], List[int]):
        annotated_sents = []
        splitting_indices = []
        for i in tqdm(range(len(df)), desc="Retrieving sentences"):
            facts, non_facts = df.loc[i, 'Facts'], df.loc[i, 'Analyses']
            facts, non_facts = facts.replace("\n", "."), non_facts.replace("\n", ".")
            facts, non_facts = self.sentence_tokeniser(facts).sents, self.sentence_tokeniser(non_facts).sents
            facts, non_facts = [(str(sent), 1) for sent in facts], [(str(sent), 0) for sent in non_facts]
            annotated_sents += facts + non_facts
            splitting_indices += [len(facts)]
        if to_file:
            sents_df = pandas.DataFrame(annotated_sents, columns=['Sentence', 'Tag'])
            sents_df.to_csv('annotated_sentences.csv', index=True, sep='\t')
        sents = [s[0] for s in annotated_sents]
        annotations = [s[1] for s in annotated_sents]
        return (sents, annotations), splitting_indices

    def inverse_transform(self, processed_outputs):
        pass
