import torch


class LSTMLinear(torch.nn.Module):
    """
    LSTM network with a linear head that takes in a camemBERT
    document representation and returns a binary sequence
    """
    def __init__(self, embedding_size):
        super(LSTMLinear, self).__init__()
        self.lstm = torch.nn.LSTM(input_size=embedding_size, hidden_size=embedding_size, batch_first=True, bidirectional=True)
        self.linear = torch.nn.Linear(in_features=2*embedding_size, out_features=1)
        self.sigma = torch.nn.Sigmoid()
        self.relu = torch.nn.ReLU()
    def forward(self, x):
        output = self.lstm(x)[0]
        output = self.relu(output)
        output = self.linear(output)
        output = self.sigma(output)
        output.squeeze_()
        return output

class GRULinear(torch.nn.Module):
    """
    GRU network with a linear head that takes in a camemBERT
    document representation and returns a binary sequence
    """
    def __init__(self, embedding_size):
        super(GRULinear, self).__init__()
        self.gru = torch.nn.GRU(input_size=embedding_size, hidden_size=embedding_size, batch_first=True, bidirectional=True)
        self.linear = torch.nn.Linear(in_features=2*embedding_size, out_features=1)
        self.sigma = torch.nn.Sigmoid()
        self.relu = torch.nn.ReLU()
    def forward(self, x):
        output = self.gru(x)[0]
        output = self.relu(output)
        output = self.linear(output)
        output = self.sigma(output)
        output.squeeze_()
        return output


class AttentionEncoder(torch.nn.Module):
    """
    Attention encoder that returns the annotation representation
    of the input sequence
    """

    def __init__(self, hidden_size):
        """
        Create the rnn cell object
        """
        super().__init__()
        self.gru = torch.nn.GRU(hidden_size, hidden_size, batch_first=True, bidirectional=True)
        self.output = torch.nn.Linear(in_features=2 * hidden_size, out_features=hidden_size)
        self.hidden_size = hidden_size

    def forward(self, input: torch.Tensor) -> torch.Tensor:
        """
        Iterate over the input in both directions gathering
        hidden states and concatenate them to create annotations
        """
        output = self.gru(input)[0]
        output = output.view(len(input[0]), len(input), 2, self.hidden_size).squeeze(1)
        forward = output[:, 0, :]
        backward = output[:, 1, :]
        backward = reversed(backward)
        output = torch.cat([forward, backward], dim=1)
        output = self.output(output)
        return output


class AttentionDecoder(torch.nn.Module):
    """
    Attention decoder that calculates the annotation weights and
    applies the attention mechanism to the decoding step
    """

    def __init__(self, hidden_size, max_len=128):
        """
        Create Linear layer for the alignment model and
        GRU network that decodes the input
        """
        super().__init__()
        self.hidden_size = hidden_size
        self.max_len = max_len
        self.attn1 = torch.nn.Linear(in_features=hidden_size, out_features=max_len, bias=False)
        self.attn2 = torch.nn.Linear(in_features=max_len, out_features=1, bias=False)
        self.gru = torch.nn.GRU(input_size=hidden_size, hidden_size=hidden_size, batch_first=True)
        self.output = torch.nn.Linear(in_features=hidden_size, out_features=1)

    def forward(self, annotations, prev_hidden):
        annotations = torch.cat([annotations, prev_hidden], dim=0)
        attn = torch.relu(self.attn1(annotations))
        attn = torch.softmax(self.attn2(attn), dim=0)
        attn_applied = attn * annotations
        output, hidden = self.gru(attn_applied.unsqueeze(0))
        hidden = hidden.squeeze(0)
        output = output.squeeze(0)
        output = self.output(output)
        output = torch.sigmoid(torch.sum(output, dim=0))
        return output, hidden


class AttentionEncoderDecoder(torch.nn.Module):
    """
    Encoder-Decoder architecture with an original implementation of the
    attention mechanism (Bahdanau 2015)
    """

    def __init__(self, hidden_size, max_len, device):
        super().__init__()
        self.max_len = max_len
        self.encoder = AttentionEncoder(hidden_size=hidden_size)
        self.decoder = AttentionDecoder(hidden_size=hidden_size, max_len=self.max_len)
        self.hidden_size = hidden_size
        self.device = device

    def forward(self, input):
        annotations = self.encoder(input)
        hidden = torch.zeros([1, self.hidden_size]).to(self.device)
        output = []
        for i in range(len(annotations)):
            sent, hidden = self.decoder(annotations, hidden)
            output.append(sent)
        output = torch.stack(output).squeeze_()
        return output
