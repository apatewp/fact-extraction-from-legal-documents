import gensim
import pandas
import spacy
import torch
# TODO separate original sents from augmented sents for validation
from main.pipelines.sentence_classifier_pipelines import GetSentencesPipeline, AugmentSentencesPipeline, \
    SentenceClassifierPipeline, CreateGoldStandardPipeline, GetOffsetsPipeline, SplitSentencesPipeline

tokeniser = spacy.load("fr_core_news_sm")
emb_model = gensim.models.KeyedVectors.load_word2vec_format(
    "frWac_non_lem_no_postag_no_phrase_200_cbow_cut100.bin",
    binary=True,
    unicode_errors='ignore'
)
df = pandas.read_csv("data/data_raw_no_dups_20200525.tsv", sep="\t")
augment_range = [0, 1, 2]
gamma_range = [0.6, 0.8, 1, 1.2, 1.4]

get_sentences_pipeline = GetSentencesPipeline(tokeniser=tokeniser)
create_goldstandard_pipeline = CreateGoldStandardPipeline()

(sents, annotations), splitting_indices = get_sentences_pipeline.transform(data_inputs=df)
documents = create_goldstandard_pipeline.transform(data_inputs=df)

for augment in augment_range:
    print("AUGMENT = {}".format(augment))
    augment_sentences_pipeline = AugmentSentencesPipeline(tokeniser=tokeniser, augment=augment)
    sentence_classifier_pipeline = SentenceClassifierPipeline(tokeniser=tokeniser, emb_model=emb_model, classify_sentences__n_epochs=1, augment=augment)
    get_offsets_pipeline = GetOffsetsPipeline()

    _, (sents, annotations) = augment_sentences_pipeline.fit_transform(data_inputs=sents, expected_outputs=annotations)
    _, prediction = sentence_classifier_pipeline.fit_transform(data_inputs=sents, expected_outputs=annotations)
    model = sentence_classifier_pipeline.steps['classify_sentences'].model
    torch.save(model.state_dict(), "test_model_augm_{}.pth".format(augment))

    split_sentences_pipeline = SplitSentencesPipeline(model=model, tokeniser=tokeniser, emb_model=emb_model, gamma=gamma_range)
    pred_indices = split_sentences_pipeline.transform(data_inputs=documents)
    offset_list = get_offsets_pipeline.transform(data_inputs=(pred_indices, splitting_indices))
    with open("offsets_augm_{}".format(augment), "a") as offsets_file:
        for i, gamma in enumerate(gamma_range):
            offsets = offset_list[i]
            offsets_file.write("GAMMA = {} ".format(gamma)+str(offsets)+"\n")
